<?php
namespace controller;
use config;
use model;

Class Controller_Event {

    public $model,$dns,$user,$pass;

    public function __construct()
    {
       $this->dns = config\ConfigDB::getDNS();
       $this->user = config\ConfigDB::$user;
       $this->pass = config\ConfigDB::$pass;
       $this->model = new model\Model_Event($this->dns,$this->user,$this->pass);
    }

    public function index() {
        $data =  $this->model->getCountries();
        $member= array();
        session_start();
        $idRow = $_SESSION['id'] ?? "";
        if ($idRow) {
            $member =  $this->model->getMember($idRow);
        }
        include( 'application/views/index_view.php' );
    }

    public function add() {
        echo ($this->model->add($_REQUEST));
    }

    public function additional() {
        $data = array('request' => $_REQUEST,'file' => $_FILES['photo']);
        echo ($this->model->additional($data));
    }

    public function members() {
        $data = $this->model->members();
        include( 'application/views/members_view.php' );
    }

}