<?php
namespace model;
use model;
use config;


class Model_Event {
    public $error = array();

    public $opt = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    function __construct($dns, $user, $pass) {
        try {
            $this->pdo = new \PDO($dns, $user, $pass, $this->opt);
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function add ($data) {
        $lastId = null;
        $id = '';
        $this->error = array();
        $re_word = '/^(\p{L}{1})+$/i';
        $re_date = '/^\d{4}-\d{2}-\d{2}$/';
        $re_empty = '/^(?=\s*\S).*$/';
        $re_number = '/^[0-9]+$/';
        $re_phone= '/\+[0-9] \(\d{3}\) \d{3}-\d{4}/';
        $re_email = '/^(([^<>()\[\]\\\\.,;:\s@"]+(\.[^<>()\[\]\\\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
        foreach ($data as $key => $value) {
            $value = htmlspecialchars($value);
            $value = trim($value);
            if ( $key =='firstname') {
                $firstname = $value;
            } elseif ($key == 'lastname') {
                $lastname = $value;
            } elseif ($key == 'birthdate') {
                $birthdate = $value;
            } elseif ($key == 'report-subject') {
                $reportSubject = $value;
            } elseif ($key == 'country') {
                $country = $value;
            } elseif ($key == 'phone') {
                $phone = $value;
            } elseif ($key == 'id') {
                $id = $value;
            } elseif ($key == 'email') {
                $email = $value;
            }
        }
        if ($id =='') {
            $stmt = $this->pdo->prepare("SELECT COUNT(id) FROM `members` WHERE email=:email");
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $count = $stmt->fetchColumn();
            if ($count) {
                $this->error[] = 'This email already exists';
            }
        }
        if(count($this->error) == 0) {
            if ($id =='') {
                $query = " INSERT INTO members(`first_name`, `last_name`, `birthdate`, `report_subject`, id_country, phone, email) VALUES(:firstname, :lastname, :birthdate, :reportSubject, :country, :phone, :email)";
                $stmt = $this->pdo->prepare($query);
                $stmt->bindParam(':firstname', $firstname);
                $stmt->bindParam(':lastname', $lastname);
                $stmt->bindParam(':birthdate', $birthdate);
                $stmt->bindParam(':reportSubject', $reportSubject);
                $stmt->bindParam(':country', $country);
                $stmt->bindParam(':phone', $phone);
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                $stmt = $this->pdo->prepare("SELECT id FROM `members` WHERE email=:email");
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                $lastId = $stmt->fetchColumn();
                session_start();
                $_SESSION['id'] = $lastId;
                session_write_close();
            } else {
                $sql = "UPDATE `members` SET `first_name`= :firstname,`last_name`=:lastname,`birthdate`=:birthdate,`report_subject`=:reportSubject,`id_country`=:country,`phone`=:phone,`email`=:email WHERE id =:id ";
                $stmt = $this->pdo->prepare($sql);
                $stmt->bindParam(':firstname', $firstname);
                $stmt->bindParam(':lastname', $lastname);
                $stmt->bindParam(':birthdate', $birthdate);
                $stmt->bindParam(':reportSubject', $reportSubject);
                $stmt->bindParam(':country', $country);
                $stmt->bindParam(':phone', $phone);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':id', $id);
                $stmt->execute();
                $lastId = $id;
            }
        }
        $data = array('error' => $this->error,'id'=> $lastId);

        return json_encode($data);
    }

    public function additional($data) {
        $this->error =  array();
        $company = '';
        $position = '';
        $about = '';
        $target_dir = "storage/";
        $types = array(
            'image/png',
            'image/jpeg',
            'image/jpeg',
            'image/jpeg',
            'image/gif',
            'image/bmp',
            'image/vnd.microsoft.icon',
            'image/tiff',
            'image/tiff',
            'image/svg+xml',
            'image/svg+xml'
        );
        foreach ($data['request'] as $key => $value) {
            $value = htmlspecialchars($value);
            $value = trim($value);
            if ($key == 'id') {
                $id = $value;
            } elseif ($key == 'company') {
                $company = $value;
            } elseif ($key == 'position') {
                $position = $value;
            } elseif ($key == 'about') {
                    $about = $value;
            }
        }

        if ($data['file']['name']) {
            $nameDB = mt_rand(1,10000000000000);
            $target_file = $target_dir.$nameDB;
            if (($data['file']['size'] > 2000000) || ($data['file']['error'] == 1) ) {
                $this->error[] = 'Your image is too large, max file size is 2 MB';
            } elseif (!in_array($data['file']['type'], $types)) {
                $this->error[] = 'This file type is incorrect, please select an image';
            } elseif (file_exists($target_file)) {
                    $this->error[] = "Sorry, file already exists.";
            } elseif (!move_uploaded_file($data['file']["tmp_name"], $target_file)) {
                $this->error[] = 'Image not uploaded';
            }
        } else {
            $nameDB = config\ConfigDB::$defultPhoto;
        }

        if (count($this->error) == 0) {
            $sql = "UPDATE `members` SET `company`= :company,`position`=:position,`about_me`=:about_me,`photo`=:photo WHERE id =:id ";
            $statement = $this->pdo->prepare($sql);
            $statement->bindValue(":id", $id);
            $statement->bindValue(":company", $company);
            $statement->bindValue(":position", $position);
            $statement->bindValue(":about_me", $about);
            $statement->bindValue(":photo", 'storage/'.$nameDB);
            $statement->execute();
            session_start();
            unset($_SESSION['id']);
        }
        return json_encode($this->error);
    }

    public function members() {
        $data = $this->pdo->query("SELECT `photo`,CONCAT(first_name,' ',last_name) as name,report_subject,`email` FROM `members` JOIN countries ON id_country = countries.id ");
        return $data->fetchAll();
    }

    public function getCountries() {
        $data = $this->pdo->query('SELECT id, name FROM countries');
        return $data->fetchAll();
    }
    public function getMember($id) {
        $sql = "SELECT * FROM members WHERE id=:id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(":id", $id);
        $statement->execute();

        return  $statement->fetch();
    }



}