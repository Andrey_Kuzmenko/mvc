<?php include 'header_view.php';
$i = 1;
?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Photo</th>
                <th>Name</th>
                <th>Report subject</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $item) { ?>
            <tr>
                <th scope="row"><?=$i++?></th>
                <td><?=$item['photo'] ?  "<img src='/".$item['photo']."'  height='80' width='80'>" : '<img src="/storage/'.config\ConfigDB::$defultPhoto.'" alt="default image" height=\'80\' width=\'80\'>' ?></td>
                <td><?=$item['name']?></td>
                <td><?=$item['report_subject']?></td>
                <td><a href="mailto:<?=$item['email']?>"><?=$item['email']?></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-5"></div>
<div class="col-md-4">
    <a class="btn btn-primary" href="/event/index" role="button">Back to form</a>
</div>
</div>