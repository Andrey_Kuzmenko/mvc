<?php
include 'header_view.php';
?>
<body>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="row">
    <div class="col-md-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.7642492898126!2d-118.3459295487083!3d34.101179380497356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf20e4c82873%3A0x14015754d926dadb!2zNzA2MCBIb2xseXdvb2QgQmx2ZCwgTG9zIEFuZ2VsZXMsIENBIDkwMDI4LCDQodCo0JA!5e0!3m2!1sru!2sua!4v1517491726077"
                width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <form action="" id="first-form" <?= $idRow ? 'hidden="hidden"' : '' ?>>
            <p style="font-size:22px">To participate in the conference, please fill out the form</p>
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <input type="text" id="id" value="<?= $idRow ?>" name="id" hidden >
            <div class="form-group" id="firstname-group">
                <label for="firstname">First Name</label><label style="color: firebrick">&nbsp;&nbsp;*</label>
                <input type="text" class="form-control" name="firstname" id="firstname" maxlength="255" value="<?= $member['first_name'] ??''?>">
            </div>
            <div class="form-group" id="lastname-group">
                <label for="lastname">Last Name</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <input type="text" class="form-control" name="lastname" id="lastname" maxlength="255" value="<?= $member['last_name'] ??''?>">
            </div>
            <div class="form-group" id="birthdate-group">
                <label for="birthdate">Birthdate</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <input type="date" class="form-control"  name="birthdate" id="birthdate" readonly value="<?= $member['birthdate'] ??''?>">
            </div>
            <div class="form-group" id="report-group">
                <label for="report-subject">Report Subject (Max 300 char)</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <input type="text" class="form-control" name="report-subject" id="report-subject" maxlength="300" value="<?= $member['report_subject'] ??''?>">
            </div>
            <div class="form-group" id="country-group">
                <label for="country">Country</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <select class="form-control" name="country" id="country">
                    <option selected="true" disabled>Select Country</option>
                    <?php foreach ($data as $country) { ?>
                    <option value="<?=$country['id']?>" <?=($country['id'] == ($member['id_country'] ??'')) ? "selected" : ""?>><?=$country['name']?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group" id="phone-group">
                <label for="phone">Phone</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <input type="text" class="form-control" name="phone" id="phone" value="<?= $member['phone'] ??''?>">
                <p>Format Phone +1 (555) 555-5555</p>
            </div>
            <div class="form-group" id="email-group">
                <label for="email">Email</label><label class="error-reqired">&nbsp;&nbsp;*</label>
                <input type="email" pattern=pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control"  name="email" id="email" value="<?= $member['email'] ??''?>">
            </div>
            <button style="float: right" type="button" class="btn btn-default" id="next-button">Next</button>
        </form>
        <form action="" id="second-form" enctype="multipart/form-data" <?= $idRow ? '' : 'hidden="hidden"' ?>>
            <input type="text" id="id-next" value="<?= $idRow ?>" name="id" hidden >
            <div class="form-group">
                <label for="company">Company:</label>
                <input type="text" class="form-control" id="company" name="company" maxlength="255">
            </div>
            <div class="form-group">
                <label for="position">Position:</label>
                <input type="text" class="form-control" id="position" name="position" maxlength="255">
            </div>
            <div class="form-group">
                <label for="about">About me: (Max 300 char)</label>
                <p><textarea id="about" class="form-control"  rows="10" cols="40" name="about" maxlength="300"></textarea></p>
            </div>
            <div class="form-group" id="photo-group">
                <label for="photo">Photo:</label> <a id="delete-photo"><img src="/images/delete-icon.png" style="float: right" height="10" width="10"></a>
                <input type="file" class="form-control" id="photo" name="photo" accept="image/*">

            </div>
            <button style="float: right" type="button" class="btn btn-default" id="second-next-button">Next</button>
            <button style="float: right" type="button" class="btn btn-default" id="prev-button">Prev</button>
        </form>
        <div id="error_second" class="alert alert-danger" role="alert" hidden="hidden"></div>
    </div>
    <div class="col-md-4"></div>
</div>

<div id="social-network" hidden="hidden">
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-1">
           <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u="><img src="/images/facebook.png" height="28" alt="Facebook"></a>
        </div>
        <div class="col-md-1">
            <p><a class="twitter popup" href="http://twitter.com/share?text=<?= htmlspecialchars(config\ConfigDB::$textShare) ?>"><img src="/images/twitter-logo.png" height="28" alt="Twitter"></a></p></a>
        </div>
        <div class="col-md-1">
            <p><a href="https://plus.google.com/share?url=<?= htmlspecialchars(config\ConfigDB::getUrlSite()) ?>"><img src="/images/new-gplus-share-button.png" height="28" alt="Google+"></a></p></a>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-1"><a class="btn btn-primary" href="/" role="button">Show Form</a></div>
        <div class="col-md-1"><a class="btn btn-primary" href="/event/members" role="button">Show members list</a></div>
    </div>
</div>
</body>
</html>
<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript">
</script>
<script>

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("contentType", "application/x-www-form-urlencoded; charset=UTF-8");
        }
    });

    (function (jQuery, window, undefined) {
        "use strict";

        var matched, browser;

        jQuery.uaMatch = function (ua) {
            ua = ua.toLowerCase();

            var match = /(opr)[\/]([\w.]+)/.exec(ua) ||
                /(chrome)[ \/]([\w.]+)/.exec(ua) ||
                /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
                /(webkit)[ \/]([\w.]+)/.exec(ua) ||
                /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
                /(msie) ([\w.]+)/.exec(ua) ||
                ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec(ua) ||
                ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
                [];

            var platform_match = /(ipad)/.exec(ua) ||
                /(iphone)/.exec(ua) ||
                /(android)/.exec(ua) ||
                /(windows phone)/.exec(ua) ||
                /(win)/.exec(ua) ||
                /(mac)/.exec(ua) ||
                /(linux)/.exec(ua) ||
                /(cros)/i.exec(ua) ||
                [];

            return {
                browser: match[3] || match[1] || "",
                version: match[2] || "0",
                platform: platform_match[0] || ""
            };
        };

        matched = jQuery.uaMatch(window.navigator.userAgent);
        browser = {};

        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
            browser.versionNumber = parseInt(matched.version);
        }

        if (matched.platform) {
            browser[matched.platform] = true;
        }

        // These are all considered mobile platforms, meaning they run a mobile browser
        if (browser.android || browser.ipad || browser.iphone || browser["windows phone"]) {
            browser.mobile = true;
        }

        // These are all considered desktop platforms, meaning they run a desktop browser
        if (browser.cros || browser.mac || browser.linux || browser.win) {
            browser.desktop = true;
        }

        // Chrome, Opera 15+ and Safari are webkit based browsers
        if (browser.chrome || browser.opr || browser.safari) {
            browser.webkit = true;
        }

        // IE11 has a new token so we will assign it msie to avoid breaking changes
        if (browser.rv) {
            var ie = "msie";

            matched.browser = ie;
            browser[ie] = true;
        }

        // Opera 15+ are identified as opr
        if (browser.opr) {
            var opera = "opera";

            matched.browser = opera;
            browser[opera] = true;
        }

        // Stock Android browsers are marked as Safari on Android.
        if (browser.safari && browser.android) {
            var android = "android";

            matched.browser = android;
            browser[android] = true;
        }

        // Assign the name and platform variable
        browser.name = matched.browser;
        browser.platform = matched.platform;


        jQuery.browser = browser;
    })(jQuery, window);

    /*
        Masked Input plugin for jQuery
        Copyright (c) 2007-2011 Josh Bush (digitalbush.com)
        Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
        Version: 1.3
      https://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js
    */
    (function (a) {
        var b = (a.browser.msie ? "paste" : "input") + ".mask", c = window.orientation != undefined;
        a.mask = {
            definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
            dataName: "rawMaskFn"
        }, a.fn.extend({
            caret: function (a, b) {
                if (this.length != 0) {
                    if (typeof a == "number") {
                        b = typeof b == "number" ? b : a;
                        return this.each(function () {
                            if (this.setSelectionRange) this.setSelectionRange(a, b); else if (this.createTextRange) {
                                var c = this.createTextRange();
                                c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select()
                            }
                        })
                    }
                    if (this[0].setSelectionRange) a = this[0].selectionStart, b = this[0].selectionEnd; else if (document.selection && document.selection.createRange) {
                        var c = document.selection.createRange();
                        a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length
                    }
                    return {begin: a, end: b}
                }
            }, unmask: function () {
                return this.trigger("unmask")
            }, mask: function (d, e) {
                if (!d && this.length > 0) {
                    var f = a(this[0]);
                    return f.data(a.mask.dataName)()
                }
                e = a.extend({placeholder: "_", completed: null}, e);
                var g = a.mask.definitions, h = [], i = d.length, j = null, k = d.length;
                a.each(d.split(""), function (a, b) {
                    b == "?" ? (k--, i = a) : g[b] ? (h.push(new RegExp(g[b])), j == null && (j = h.length - 1)) : h.push(null)
                });
                return this.trigger("unmask").each(function () {
                    function v(a) {
                        var b = f.val(), c = -1;
                        for (var d = 0, g = 0; d < k; d++) if (h[d]) {
                            l[d] = e.placeholder;
                            while (g++ < b.length) {
                                var m = b.charAt(g - 1);
                                if (h[d].test(m)) {
                                    l[d] = m, c = d;
                                    break
                                }
                            }
                            if (g > b.length) break
                        } else l[d] == b.charAt(g) && d != i && (g++, c = d);
                        if (a || c + 1 >= i) u(), a || f.val(f.val().substring(0, c + 1));
                        return i ? d : j
                    }

                    function u() {
                        return f.val(l.join("")).val()
                    }

                    function t(a, b) {
                        for (var c = a; c < b && c < k; c++) h[c] && (l[c] = e.placeholder)
                    }

                    function s(a) {
                        var b = a.which, c = f.caret();
                        if (a.ctrlKey || a.altKey || a.metaKey || b < 32) return !0;
                        if (b) {
                            c.end - c.begin != 0 && (t(c.begin, c.end), p(c.begin, c.end - 1));
                            var d = n(c.begin - 1);
                            if (d < k) {
                                var g = String.fromCharCode(b);
                                if (h[d].test(g)) {
                                    q(d), l[d] = g, u();
                                    var i = n(d);
                                    f.caret(i), e.completed && i >= k && e.completed.call(f)
                                }
                            }
                            return !1
                        }
                    }

                    function r(a) {
                        var b = a.which;
                        if (b == 8 || b == 46 || c && b == 127) {
                            var d = f.caret(), e = d.begin, g = d.end;
                            g - e == 0 && (e = b != 46 ? o(e) : g = n(e - 1), g = b == 46 ? n(g) : g), t(e, g), p(e, g - 1);
                            return !1
                        }
                        if (b == 27) {
                            f.val(m), f.caret(0, v());
                            return !1
                        }
                    }

                    function q(a) {
                        for (var b = a, c = e.placeholder; b < k; b++) if (h[b]) {
                            var d = n(b), f = l[b];
                            l[b] = c;
                            if (d < k && h[d].test(f)) c = f; else break
                        }
                    }

                    function p(a, b) {
                        if (!(a < 0)) {
                            for (var c = a, d = n(b); c < k; c++) if (h[c]) {
                                if (d < k && h[c].test(l[d])) l[c] = l[d], l[d] = e.placeholder; else break;
                                d = n(d)
                            }
                            u(), f.caret(Math.max(j, a))
                        }
                    }

                    function o(a) {
                        while (--a >= 0 && !h[a]) ;
                        return a
                    }

                    function n(a) {
                        while (++a <= k && !h[a]) ;
                        return a
                    }

                    var f = a(this), l = a.map(d.split(""), function (a, b) {
                        if (a != "?") return g[a] ? e.placeholder : a
                    }), m = f.val();
                    f.data(a.mask.dataName, function () {
                        return a.map(l, function (a, b) {
                            return h[b] && a != e.placeholder ? a : null
                        }).join("")
                    }), f.attr("readonly") || f.one("unmask", function () {
                        f.unbind(".mask").removeData(a.mask.dataName)
                    }).bind("focus.mask", function () {
                        m = f.val();
                        var b = v();
                        u();
                        var c = function () {
                            b == d.length ? f.caret(0, b) : f.caret(b)
                        };
                        (a.browser.msie ? c : function () {
                            setTimeout(c, 0)
                        })()
                    }).bind("blur.mask", function () {
                        v(), f.val() != m && f.change()
                    }).bind("keydown.mask", r).bind("keypress.mask", s).bind(b, function () {
                        setTimeout(function () {
                            f.caret(v(!0))
                        }, 0)
                    }), v()
                })
            }
        })
    })(jQuery);

    $('document').ready(function () {
        $("#phone").mask("+9 (999) 999-9999", {autoclear: false});
    });

    var error = 0;
    $("#birthdate").datepicker({
        dateFormat: 'yy-dd-mm',
        changeMonth: true,
        changeYear: true,
        maxDate: new Date,
        minDate: new Date(1900, 1, 1)
    });

    errorAjax = new Array();
    $('#next-button').click(function () {
       if ($('#firstname').val() == '') {
           if ($('#error-firstname').length == 0) {
               $('#firstname-group').addClass('has-error');
               $('#firstname').after('<p id="error-firstname" class="error-message">This field is required</p>');
           }
           error++;
        } else if ($('#error-firstname').length > 0) {
           $('#error-firstname').detach();
           $('#firstname-group').removeClass('has-error');
        }

        if ($('#lastname').val() == '') {
           $('#error-lastname').detach();
           $('#lastname-group').addClass('has-error');
           $('#lastname').after('<p id="error-lastname" class="error-message">This field is required</p>');
            error++;
        } else if ($('#error-lastname').length > 0) {
            $('#lastname-group').removeClass('has-error');
            $('#error-lastname').detach();
        }
        birthdate = $('#birthdate').val();
        var dateRegex = /[0-9]{4}-[0-9]{2}-[0-9]{2}$/g;
        if (dateRegex.exec(birthdate) == null) {
           if (birthdate == '') {
               $('#error-birthdate').detach();
               $('#birthdate-group').addClass('has-error');
               $('#birthdate').after('<p id="error-birthdate" class="error-message">This field is required</p>');
               error++;
           } else {
               $('#error-birthdate').detach();
               $('#birthdate-group').addClass('has-error');
               $('#birthdate').after('<p id="error-birthdate" class="error-message">Birthdate must be a date</p>');
           }
        } else {
                if ($('#error-birthdate').length > 0) {
                    $('#birthdate-group').removeClass('has-error');
                    $('#error-birthdate').detach();
                }
            }
        if ($('#report-subject').val() == '') {
            if ($('#error-report-subject').length == 0) {
                $('#report-group').addClass('has-error');
                $('#report-subject').after('<p id="error-report-subject" class="error-message">This field is required</p>');
                error++;
            }
        } else {
            if ($('#error-report-subject').length > 0) {
                $('#report-group').removeClass('has-error');
                $('#error-report-subject').detach();
            }
        }
        if ($('#country').val() == null) {
            if ($('#error-country').length == 0) {
                $('#country-group').addClass('has-error');
                $('#country').after('<p id="error-country" class="error-message">This field is required</p>');
            }
            error++;
        } else if ($('#error-country').length > 0) {
            $('#error-country').detach();
            $('#country-group').removeClass('has-error');
        }
        regex = /\+[0-9] \(\d{3}\) \d{3}-\d{4}/g;
        number = $('#phone').val();
        if (regex.exec(number) == null) {
                if (number == '') {
                    $('#error-phone').detach();
                    $('#phone-group').addClass('has-error');
                    $('#phone').after('<p id="error-phone" class="error-message">This field is required</p>');
                } else {
                    $('#error-phone').detach();
                    $('#phone-group').addClass('has-error');
                    $('#phone').after('<p id="error-phone" class="error-message">Phone must satisfy the format</p>');
                }
            error++;
        } else {
            if ($('#error-phone').length > 0) {
                $('#phone-group').removeClass('has-error');
                $('#error-phone').detach();
            }
        }
        regex_email = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        email = $('#email').val();
        if ($('#email').val() == '') {
                $('#error-email').detach();
                $('#email-group').addClass('has-error');
                $('#email').after('<p id="error-email" class="error-message">This field is required</p>');
                error++;
        } else if (regex_email.exec(email) == null) {
            $('#error-email').detach();
            $('#email-group').addClass('has-error');
            $('#email').after('<p id="error-email" class="error-message">Please enter a valid email address</p>');
            error++;
        }
        else {
            if ($('#error-email').length > 0) {
                $('#email-group').removeClass('has-error');
                $('#error-email').detach();
            }
        }
        if (error == 0) {
            var formFirst = document.getElementById('first-form');
            var formData1 = new FormData(formFirst);
            $.ajax({
                type: 'POST',
                url: '/event/add',
                data: formData1,
                processData: false,
                contentType: false,
                success: function (result) {
                    response = JSON.parse(result);
                    errorAjax = response['error'];
                    if (errorAjax.length == 0) {
                        $('#first-form').hide();
                        $('#second-form').show();
                        $('#email-group').removeClass('has-error');
                        $('#error-email').detach();
                        $('#id-next').attr('value', response['id']);
                        $('#id').attr('value', response['id']);
                    } else {
                        for (var i = 0; i < 1; i++) {
                            $('#error-email').detach();
                            $('#email-group').addClass('has-error');
                            $('#email').after('<p id="error-email" class="error-message">'+errorAjax[i]+'</p>');
                        }
                    }

                }
            });
        }
        error = 0;
    });
    $('#second-next-button').click(function () {
        $('#second-next-button').attr("disabled", "disabled");
        var form = document.getElementById('second-form');
        var formData = new FormData(form);
        $.ajax({
            type: 'POST',
            url: '/event/additional',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (errorAjax) {
                if (errorAjax.length == 0) {
                    $('#second-form').hide();
                    $('#photo-group').removeClass('has-error');
                    $('#error-photo').detach();
                    $('#social-network').show();
                    $('#second-next-button').removeAttr("disabled");
                } else {
                    for (var i = 0; i < errorAjax.length; i++) {
                        $('#error-photo').detach();
                        $('#photo-group').addClass('has-error');
                        $('#photo').after('<p id="error-photo" class="error-message">'+errorAjax[i]+'</p>');
                    }
                    $('#error').show();
                    setTimeout(function () {
                        $('#second-next-button').removeAttr("disabled");
                    }, 3000);
                    }

            }

        });

    });
    $('#prev-button').click(function () {
        $('#second-form').hide();
        $('#first-form').show();
    });
    $('#delete-photo').click(function () {
        $('#photo').val('');
    });

</script>